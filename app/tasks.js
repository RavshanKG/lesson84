const express = require('express');

const Task = require('../models/Task');
const auth = require('../middleware/auth');

const router = express.Router();

const createRouter = () => {
  router.get('/', auth, (req, res) => {
    Task.find({user: req.user._id})
      .then(results => res.send(results))
      .catch(error => res.status(500).send(error))
  });

  router.post('/', auth, (req, res) => {
    const taskData = req.body;
    taskData.user = req.user._id;

    // if (!taskData.status) {
    //   taskData.status = 'new';     Заменил на ключ default и убрал required
    // }

    const task = new Task(taskData);

    task.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.put('/:id', auth, async (req, res) => {
    const task = await Task.findById(req.params.id);

    task.title = req.body.title || task.title;
    task.description = req.body.description || task.description;
    task.status = req.body.status || task.status;

    task.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.delete('/:id', auth, (req, res) => {
    Task.findByIdAndRemove(req.params.id)
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;